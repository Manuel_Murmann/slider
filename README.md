See it in action: https://murmannmanuel.de/slider

Simple work in progress slider with not much functionality

I built this because when i tried libraries for sliders i thought they were really confusing and i needed the training.

**What it can do:**

- You can decide how many slides you want to show at a time \n
- You can decide how many slides you want to scroll at a time
- It works for touchscreens and snaps to the most centered slide
- It has radio buttons which can also be used for scrolling
- Infinite scrolling, it snaps back to the first / last slide when reaching the last / first slide
- Radio buttons are created automatically


**What i plan on doing:**

- Make it draggable for desktops
- Add a blur to the both sides of the slider so the slides dont just disappear


